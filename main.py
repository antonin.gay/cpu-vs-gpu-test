from time import time

# noinspection PyUnresolvedReferences
import ipykernel
import numpy as np
from tensorflow.keras import Sequential, Input, Model
from tensorflow.keras.layers import Dense

# Parameters
n_samples = 100000
input_dim = 1000
output_dim = 10

n_layers = 20
n_units = 512

batch_size = 2048
epochs = 50

# Data
print('Creating data')
inputs = np.random.random((n_samples, input_dim))
outputs = np.random.random((n_samples, output_dim))

# Neural net
print('Creating Network')

inp_nn = Input(shape=(input_dim,))

layers = Sequential()
for _ in range(n_layers):
    layers.add(Dense(units=n_units, activation='relu'))

layers.add(Dense(units=output_dim, activation='relu'))
out_nn = layers(inp_nn)

neural_net = Model(inp_nn, out_nn)

print('Creating Network')
neural_net.compile('adam', 'mse')

t0 = time()
neural_net.fit(x=inputs, y=outputs, batch_size=batch_size, epochs=epochs, validation_split=0.2, verbose=1)

print('DataSet:'
      '\n\tNumber of samples: %d'
      '\n\tInput Dimension: %d'
      '\nNetwork:'
      '\n\tNumber of hidden layers: %d'
      '\n\tNeurons per hidden layers: %d'
      '\nTraining:'
      '\n\tEpochs: %d'
      '\n\tBatch Size: %d'
      % (n_samples, input_dim, n_layers, n_units, epochs, batch_size))
print('\nTotal Fitting time on GPU: %.2fs' % (time() - t0))
